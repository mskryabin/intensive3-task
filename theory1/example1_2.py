class Client:
    def __init__(self, name, info):
        self.name = name
        self.info = info

    def display(self):
        print('Имя:', self.name)
        print('Контактная информация:', self.info)


class CorporateClient(Client):
    def display(self):
        print('Название:', self.name)
        print('Контактная информация:', self.info)


c1 = Client('Иванов Михаил', '')
c2 = CorporateClient('ООО "Ромашка"', 'info@romashka.ru')
print(c1.display())
print(c2.display())
