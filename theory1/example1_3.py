class Client:
    def __init__(self, name, info):
        self.name = name
        self.info = info

    def display(self):
        print('Имя:', self.name)
        print('Контактная информация:', self.info)


class CorporateClient(Client):
    def __init__(self, name, inn, info):
        self.name = name
        self.info = info
        self.inn = inn

    def display(self):
        print('Название:', self.name)
        print('ИНН:', self.inn)
        print('Контактная информация:', self.info)


c1 = Client('Иванов Михаил', '')
c2 = CorporateClient('ООО "Ромашка"', '271121122112', 'info@romashka.ru')
print(c1.display())
print(c2.display())
