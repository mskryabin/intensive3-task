# Напишите декоратор check_permission, который проверяет,
# есть ли у пользователя доступ к вызываемой функции,
# и если нет, то выдаёт исключение PermissionError.

# Пример кода:

user_permissions = ['owner']


@check_permission('owner')
def delete_site():
    print('Удаляем сайт')

@check_permission('user')
def add_comment():
    print('Добавляем комментарий')


delete_site()  # Удаляем сайт
add_comment()  # PermissionError: У пользователя недостаточно прав, чтобы выполнить функцию add_comment
