# 2. Углубленная работа с классами
## Свойства @property
Декоратор `@property` облегчает создание свойств в классах Python. 
Свойства выглядят как обычные атрибуты (поля) класса, 
но при их чтении вызывается геттер (getter), при записи – сеттер (setter), 
а при удалении – делитер (deleter). 

Свойства нужны, чтобы в стиле ООП обрабатывать работу с полями класса. 
В ООП с полями класса не работают напрямую, они скрыты. 
Для взаимодействия выводятся геттеры и сеттеры. 
Кроме того, геттер может производить вычисления, 
если свойство не хранится в классе (вычислимые поля). 
А сеттер может проверять входные данные на корректность.

__Пример.__ Верификация полей
```python
class User:
    def __init__(self, name):
        if not name:
            raise ValueError('empty name')
        self.__name = name
    
    @property
    def name(self):
        return self.__name
    
    @name.setter
    def name(self, new_name):
        if not new_name:
            raise ValueError('empty name')
        self.__name = new_name

user = User('Максим')
user.name = 'Сергей'
print(user.name)  # Сергей
user.name = ''    # ValueError
```

__Пример.__ Вычислимое свойство
```python
class Circle:
    def __init__(self, r):
        self.r = r
    
    @property
    def area(self):
        return 3.1415 * self.r**2

c = Circle(10)
print(c.area)
```

## Статические и классовые методы
Методы могут быть не только у экземпляра класса, 
но и у самого класса, которые вызываются без какого-то экземпляра (без `self`). 
Декораторы `@staticmethod` и `@classmethod` как раз делают метод таким 
(статическим или классовым). 

Статический метод – это способ поместить функцию в класс, 
если она логически относится к этому классу. Статический метод ничего не знает 
о классе, из которого его вызвали.
```python
class Foo:
    @staticmethod
    def doc():
        print('documentation for Foo class')

Foo.doc()
```

Классовый метод напротив знает, из какого класса его вызывают. 
Он принимает неявный первый аргумент (обычно его зовут `cls`), 
который содержит вызывающий класс. Классовые методы прекрасно подходят, 
когда нужно учесть иерархию наследования. 
Пример: метод `group` создает список из нескольких клиентов. 
Причем для `Client` – список `Client`, а для `CorporateClient` – 
список `CorporateClient`. 
Со `@staticmethod` такое бы не вышло.
```python
class Client:
    @classmethod
    def group(cls, n):
        # cls именно тот класс, который вызвал
        return [cls() for _ in range(n)]
    
    def __repr__(self):
        return 'Client'
    

class CorporateClient(Client):
    def __repr__(self):
        return 'Corporate Client'


print(Client.group(3))
# [Client, Client, Client]
print(CorporateClient.group(2))
# [Corporate Client, Corporate Client]
```

## Создание собственных декораторов
Декоратор — это функция, которая позволяет 
обернуть другую функцию для расширения её функциональности 
без непосредственного изменения её кода.

Посмотрим на пример декоратора:
```python
def decorator_function(func):
    def wrapper():
        print('Функция-обёртка!')
        print('Оборачиваемая функция: {}'.format(func))
        print('Выполняем обёрнутую функцию...')
        func()
        print('Выходим из обёртки')
    return wrapper
```
Вот как декоратор работает.
```python
@decorator_function
def hello_world():
    print('Hello world!')

hello_world()
```

Создадим декоратор, замеряющий время выполнения функции.
```python
def benchmark(func):
    import time

    def wrapper(*args, **kwargs):
        start = time.time()
        return_value = func(*args, **kwargs)
        end = time.time()
        print('[*] Время выполнения: {} секунд.'.format(end - start))
        return return_value
    return wrapper

@benchmark
def fetch_webpage(url):
    import requests
    webpage = requests.get(url)
    return webpage.text

webpage = fetch_webpage('https://google.com')
```

Мы также можем создавать декораторы, которые принимают аргументы. Посмотрим на пример:
```python
def benchmark(iters):
    def actual_decorator(func):
        import time

        def wrapper(*args, **kwargs):
            total = 0
            return_value = None
            for i in range(iters):
                start = time.time()
                return_value = func(*args, **kwargs)
                end = time.time()
                total = total + (end - start)
            print('[*] Среднее время выполнения: {} секунд.'.format(total / iters))
            return return_value

        return wrapper
    return actual_decorator


@benchmark(iters=10)
def fetch_webpage(url):
    import requests
    webpage = requests.get(url)
    return webpage.text

webpage = fetch_webpage('https://google.com')

```
Здесь мы модифицировали наш старый декоратор таким образом, чтобы он выполнял декорируемую функцию iters раз, а затем выводил среднее время выполнения. 
Однако чтобы добиться этого, пришлось воспользоваться природой функций в Python.

Декорировать можно и методы классов.
Функции и методы в Python — это практически одно и то же, 
за исключением того, что методы всегда ожидают первым параметром ссылку на сам объект (self). Это значит, что мы можем создавать декораторы для методов точно так же, 
как и для функций, просто не забывая про self.

```python
def track(func):
    def wrapper(self):
        print("Run method: " + str(func.__name__))
        func(self)
        return wrapper

class User:
    def __init__(self, name):
        self.name = name
    
    @track
    def display(self):
        print('Имя:', self.name)

user = User('Вася')
user.display()
```
