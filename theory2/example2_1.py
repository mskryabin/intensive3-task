class User:
    def __init__(self, name):
        if not name:
            raise ValueError('empty name')
        self.__name = name

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, new_name):
        if not new_name:
            raise ValueError('empty name')
        self.__name = new_name


user = User('Максим')
user.name = 'Сергей'
print(user.name)  # Сергей
user.name = ''    # ValueError
