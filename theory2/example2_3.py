class Client:
    @classmethod
    def group(cls, n):
        # cls именно тот класс, который вызвал
        return [cls() for _ in range(n)]

    def __repr__(self):
        return 'Client'


class CorporateClient(Client):
    def __repr__(self):
        return 'Corporate Client'


print(Client.group(3))
# [Client, Client, Client]
print(CorporateClient.group(2))
# [Corporate Client, Corporate Client]
