def track(func):
    def wrapper(self):
        print("Run method: " + str(func.__name__))
        func(self)
        return wrapper

class User:
    def __init__(self, name):
        self.name = name

    @track
    def display(self):
        print('Имя:', self.name)


user = User('Вася')
user.display()
