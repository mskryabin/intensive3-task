# Добавьте в класс SortedList, в котором элементы всегда отсортированы по возрастанию,
# методы append(value) и pop(key)

# Пример кода

my_list = SortedList([3, 1, 2])
my_list.append(1.5)
print(my_list)  # [1, 1.5, 2, 3]
my_list.pop(1)
print(my_list)  # [1, 2, 3]
